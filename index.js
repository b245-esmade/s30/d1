// Aggregation in MongoDB and Query Case Studies
// Process MongoDB documents and return computed results


	// Aggregrate - cluster of things that have come or have been brought together
	// Aggregation Pipeline Stages
			// $match
			// $group


//[SECTION] MongoDB Aggregration
	// to generate and perform operations to create filtered results that helps us analyze the data
	// using aggregate method:
		/*
			$match is used to pass the documents that meet the specified condition to next stage of aggregation process

			Syntax:
				{$match: {field:value}}
		*/

		db.fruits.aggregate([{$match: {onSale: true}}]);


		/*
			$group is used to group elements together and field-value pairs the data from the group element

			Syntax:
				{$group: { _id://fieldSetGroup,}}
		*/

		db.fruits.aggregate([
			{$match: {onSale: true}},
			{$group:{_id:"$supplier_id", totalFruits:{$sum:"stock"}}}

			]);


db.fruits.aggregate([
	{$match: {color: "Yellow"}},
	{$group:{_id:"$supplier_id",
	totalFruits:{$sum:"stock"}}}
]);


db.fruits.aggregate([ 
    {$match: {color: "Yellow"} },
    {$group: {_id: "$supplier_id", 
	totalFruits: {$sum: "$stock"}} }
]);



		// Field Projection with aggregation
		/*
			$project can be used when aggregating data to include/exclude from the returned result

			Syntax:
				{$project: {field:1/0}}


		*/

db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group:{_id:"$supplier_id",
		totalFruits:{$sum:"$stock"}}},
		]);




		// Sorting aggregated results
		/*
			$sort can be used to change the order of the aggregated result

			Syntax:
				{$project: {field:1/0}}


		*/

db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group:{_id:"$supplier_id",
		totalFruits:{$sum:"$stock"}}},
		{$sort: {totalFruits:1}}
		]);

	// 1 lowest to highest
	// 2 highest to lowest


			// Aggregating results based on an array fields
			/*
				$unwind can be used 

			*/


db.fruits.aggregate([
	{$unwind: "$origin"},
	{$group: {_id:"$origin", fruits: {$sum:"$stock"}}}

	]);


		//[SECTION] Other aggregate stages
			/*
					$count all values

			*/

db.fruits.aggregate([
		{$match: {color:"Yellow"}},
		{$count: "Yellow Fruits"},

	])


		//$avg gets the average value of the stock

		/*
			$avg gets the average value of the stock

		*/


db.fruits.aggregate([
	{$match: {color: "Yellow"}},
	{$group: {_id: "$color", avgYellow: {$avg: "$stock"}}}

	])



		//$min && $max 

		/*
			$min && $max 
			minimum && maximum

		*/



db.fruits.aggregate([
	{$match: {color: "Yellow"}},
	{$group: {_id: "$color", lowestStock: {$min: "$stock"}}},
	])


db.fruits.aggregate([
	{$match: {color: "Yellow"}},
	{$group: {_id: "$color", highestStock: {$max: "$stock"}}},
	])

